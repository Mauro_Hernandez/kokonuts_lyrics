package com.kokonutstudio.kokonutstudio.data.Lyrics

import com.kokonutstudio.kokonutstudio.models.Lyrics
import com.kokonutstudio.kokonutstudio.models.SuggestLyrics
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface ApiserviceListLyrics {

    @GET("suggest/{query}")
    fun getSuggest( @Path("query")query : String):Call<SuggestLyrics>

    @GET("v1/{Artist}/{Song} ")
    fun getLyrics( @Path("Artist") artist : String, @Path("Song") song : String ):Call<Lyrics>
}