package com.kokonutstudio.kokonutstudio.data.Lyrics

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kokonutstudio.kokonutstudio.exceptions.Exceptions
import com.kokonutstudio.kokonutstudio.models.Lyrics
import com.kokonutstudio.kokonutstudio.models.SuggestLyrics
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class ListLyricsDataset {

    lateinit var service: ApiserviceListLyrics
    var suggestLyrics:SuggestLyrics?=null
    var lyrics:Lyrics?=null
    var error:Lyrics?=null



    fun setupServive(){
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://api.lyrics.ovh/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create<ApiserviceListLyrics>(ApiserviceListLyrics::class.java)

    }

    suspend fun searchSuggest(query: String):Unit= suspendCancellableCoroutine{ continuar->
        setupServive()
            service.getSuggest(query).enqueue(object: Callback<SuggestLyrics> {
                override fun onResponse(call: Call<SuggestLyrics>, response: Response<SuggestLyrics>) {
                    suggestLyrics=response.body()
                    continuar.resume(Unit)
                }
                override fun onFailure(call: Call<SuggestLyrics>, t: Throwable) {
                        continuar.resumeWithException(Exceptions(t.message.toString()))
                }
            })



    }
    suspend fun searchsLyrics(artist: String,song :String):Unit= suspendCancellableCoroutine{continuar->
        setupServive()
        service.getLyrics(artist,song).enqueue(object: Callback<Lyrics> {
            override fun onResponse(call: Call<Lyrics>, response: Response<Lyrics>) {
                lyrics=response.body()
                if(lyrics == null){
                    val gson = Gson()
                    val type = object : TypeToken<Lyrics>() {}.type
                    var errorResponse: Lyrics? = gson.fromJson(response.errorBody()!!.charStream(), type)
                    if(errorResponse!!.error != null){
                        lyrics=errorResponse
                    }
                }
                continuar.resume(Unit)
            }
            override fun onFailure(call: Call<Lyrics>, t: Throwable) {
                continuar.resumeWithException(Exceptions(t.message.toString()))
            }
        })
    }


    fun getListSuggestLyrics():SuggestLyrics{
        return suggestLyrics!!
    }
    fun getLyricsSong():Lyrics{
        return lyrics!!
    }

}