package com.kokonutstudio.kokonutstudio

import android.content.Context
import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.kokonutstudio.kokonutstudio.utils.NetworkConnetion
import kotlinx.android.synthetic.main.activity_main_kokonuts.*

class MainKokonutsActivity : AppCompatActivity() {


    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_kokonuts)
        navController= findNavController(R.id.nav_host_fragment)
        val networkConnection =NetworkConnetion(this)

        networkConnection.observe(this, Observer { isConnected ->
            isConnected?.let {
                if(it){
                    linear_NoInternet.visibility= View.GONE
                }else{
                    linear_NoInternet.visibility= View.VISIBLE
                }
            }
        })

    }

}
