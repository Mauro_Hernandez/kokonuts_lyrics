package com.kokonutstudio.kokonutstudio.domain.Lyrics

import com.kokonutstudio.kokonutstudio.data.Lyrics.ListLyricsDataset
import com.kokonutstudio.kokonutstudio.models.Lyrics
import com.kokonutstudio.kokonutstudio.models.SuggestLyrics

class ListLyricsUseCase {
    private val articlesDataset=ListLyricsDataset()

    suspend fun searchSuggest(query: String){
       articlesDataset.searchSuggest(query)
    }

    fun getListArticles():SuggestLyrics{
        return articlesDataset.getListSuggestLyrics()
    }

    suspend fun searchLyrics(artist: String, song: String){
        articlesDataset.searchsLyrics(artist,song)
    }

    fun getLyricsSong():Lyrics{
        return articlesDataset.getLyricsSong()
    }

}