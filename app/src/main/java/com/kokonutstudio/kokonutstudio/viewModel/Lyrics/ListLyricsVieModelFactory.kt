package com.kokonutstudio.kokonutstudio.viewModel.Lyrics

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kokonutstudio.kokonutstudio.domain.Lyrics.ListLyricsUseCase

class ListLyricsVieModelFactory(val listLyricsUsecase:ListLyricsUseCase):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(ListLyricsUseCase::class.java).newInstance(listLyricsUsecase)
    }
}