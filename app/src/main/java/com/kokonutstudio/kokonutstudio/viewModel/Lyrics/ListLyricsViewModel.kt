package com.kokonutstudio.kokonutstudio.viewModel.Lyrics

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kokonutstudio.kokonutstudio.domain.Lyrics.ListLyricsUseCase
import com.kokonutstudio.kokonutstudio.exceptions.Exceptions
import com.kokonutstudio.kokonutstudio.models.Data
import com.kokonutstudio.kokonutstudio.models.Lyrics
import com.kokonutstudio.kokonutstudio.models.SuggestLyrics
import com.kokonutstudio.kokonutstudio.ui.Lyrics.ListLyricsFragment
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ListLyricsViewModel(val listLyricsUseCase: ListLyricsUseCase):ViewModel(), CoroutineScope {
    private var view:ListLyricsFragment?=null
    private val listSuggest= MutableLiveData<SuggestLyrics>()
    private var lyrics:Lyrics? = null
    val job=Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main+job


    fun getListaArticles(query: String){
        launch{
            try{
                view?.showProgressBar()
                listLyricsUseCase.searchSuggest(query)
                if(listLyricsUseCase.getListArticles().total!= 0){
                    setListSuggest(listLyricsUseCase.getListArticles())
                }else{
                    view?.showMessageLyrics("No suggestions found")
                }
                view?.hideProgressBar()
            }catch (e: Exceptions){
                view?.showException(e.message.toString())
                view?.hideProgressBar()
            }

        }

    }

    fun searchsLyrics(data:Data){
        launch{
            try{
                view?.showProgressBar()
                listLyricsUseCase.searchLyrics(data.artist.name,data.titleShort)
                if(listLyricsUseCase.getLyricsSong()!= null) {
                    lyrics = listLyricsUseCase.getLyricsSong()
                    if(lyrics!!.lyrics != null)
                        view?.goAllLyrics(data,lyrics!!)
                    else
                        view?.showMessageLyrics(lyrics!!.error)
                }
                view?.hideProgressBar()
            }catch (e: Exceptions){
                view?.showException(e.message.toString())
                view?.hideProgressBar()
            }

        }
    }



    fun setListSuggest(listArticles:SuggestLyrics){
        this.listSuggest.value=listArticles
    }
    fun cleanListSuggest(){
        this.listSuggest.value=SuggestLyrics()
    }

    fun getListArticlesLiveData(): LiveData<SuggestLyrics> {
        return listSuggest
    }

    fun attachView(view:ListLyricsFragment){
        this.view=view
    }
    fun dettachView(){
        this.view=null
    }

    fun dettachJob(){
        coroutineContext.cancel()
    }
}