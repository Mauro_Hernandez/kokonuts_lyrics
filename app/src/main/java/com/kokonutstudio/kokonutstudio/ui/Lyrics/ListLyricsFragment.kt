package com.kokonutstudio.kokonutstudio.ui.Lyrics

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kokonutstudio.kokonutstudio.R
import com.kokonutstudio.kokonutstudio.domain.Lyrics.ListLyricsUseCase
import com.kokonutstudio.kokonutstudio.models.Data
import com.kokonutstudio.kokonutstudio.models.Lyrics
import com.kokonutstudio.kokonutstudio.models.SuggestLyrics
import com.kokonutstudio.kokonutstudio.ui.Lyrics.AdapterListLyrics.AdapterCardviewListLyrics
import com.kokonutstudio.kokonutstudio.utils.CustomProgressBar
import com.kokonutstudio.kokonutstudio.viewModel.Lyrics.ListLyricsVieModelFactory
import com.kokonutstudio.kokonutstudio.viewModel.Lyrics.ListLyricsViewModel
import kotlinx.android.synthetic.main.fragment_list_lyrics.*

class ListLyricsFragment : Fragment() {
    private lateinit var viewModel: ListLyricsViewModel
    private var recyclerView: RecyclerView?=null
    lateinit var navController: NavController
    val progressBar = CustomProgressBar()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_list_lyrics, container, false)
        recyclerView=view.findViewById(R.id.rv_articles_listArticles)
        setupInicioFragentViewModel()
        setObservableViewModel()

        return view!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController= Navigation.findNavController(view)
        setupSearchsView()
    }

    fun setupSearchsView(){
        searchs_list.queryHint = "Search"
        searchs_list.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                viewModel.getListaArticles(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if(newText==""){
                    viewModel.cleanListSuggest()
                    tv_message.visibility=View.VISIBLE
                }else{
                    tv_message.visibility=View.INVISIBLE
                }
                return false
            }
        })
    }

    fun setupInicioFragentViewModel(){
        viewModel= ViewModelProviders.of(this, ListLyricsVieModelFactory(ListLyricsUseCase())).get(ListLyricsViewModel::class.java)
        viewModel.attachView(this)
        //
    }

    fun setObservableViewModel(){
        val suggestObserver = Observer<SuggestLyrics>{
            updateRecyclerView(it)
        }
        viewModel.getListArticlesLiveData().observe(this,suggestObserver)
    }

    fun updateRecyclerView(list:SuggestLyrics ){
        var adapter= AdapterCardviewListLyrics(activity!!.applicationContext,list.data!!,this)
        recyclerView?.layoutManager= LinearLayoutManager(activity)
        recyclerView?.adapter=adapter
    }

    fun showProgressBar(){
        progressBar.show(activity!!,"Espera por favor...")
    }
    fun hideProgressBar(){
        progressBar.dialog.dismiss()
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.dettachJob()
        viewModel.dettachView()
    }

    fun showException(message:String){
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    }

    fun showMessageLyrics(message:String){
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()

    }

    fun goAllLyrics(data:Data, lyrics : Lyrics){
        val articleAux=ListLyricsFragmentDirections.actionListArticlesFragmentToArticleFragment(data!!,lyrics)
        navController.navigate(articleAux!!)
    }


    fun searchLyrics(data:Data){
        viewModel.searchsLyrics(data)
    }




}
