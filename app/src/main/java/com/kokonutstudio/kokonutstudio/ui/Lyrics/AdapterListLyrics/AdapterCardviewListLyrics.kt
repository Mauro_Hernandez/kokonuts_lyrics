package com.kokonutstudio.kokonutstudio.ui.Lyrics.AdapterListLyrics

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kokonutstudio.kokonutstudio.R
import com.kokonutstudio.kokonutstudio.models.Data
import com.kokonutstudio.kokonutstudio.ui.Lyrics.ListLyricsFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cardview_article.view.*

class AdapterCardviewListLyrics(var context: Context, listData:List<Data>, view:ListLyricsFragment): RecyclerView.Adapter<AdapterCardviewListLyrics.AdapterViewHolder>() {
    var view:ListLyricsFragment?=view
    var listData:List<Data>?=listData
    var viewHolder:AdapterViewHolder?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val v=LayoutInflater.from(parent?.context).inflate(R.layout.cardview_article,parent,false)
        viewHolder= AdapterViewHolder(v)
        return viewHolder!!

    }

    override fun getItemCount(): Int {
        return listData?.size!!
    }

    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {
        holder.description.text=listData?.get(position)?.title.toString()

        Picasso
            .get()
            .load(listData?.get(position)?.artist?.picture)
            .error(R.drawable.imagen_no_disponible_)
            .into(holder.img)

        holder.img.setOnClickListener {
            view?.searchLyrics(listData?.get(position)!!)
        }
        holder.description.setOnClickListener {
           view?.searchLyrics(listData?.get(position)!!)
        }
    }

    class AdapterViewHolder(view:View):RecyclerView.ViewHolder(view!!){
        val img=view.cv_img
        val description=view.tv_Description_Card
    }
}