package com.kokonutstudio.kokonutstudio.ui.Lyrics


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation

import com.kokonutstudio.kokonutstudio.R
import com.kokonutstudio.kokonutstudio.models.Data
import com.kokonutstudio.kokonutstudio.models.Lyrics
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_lyrics.*

class LyricsFragment : Fragment() {
    lateinit var data: Data
    lateinit var lyrics: Lyrics
    lateinit var navController: NavController


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lyrics, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController= Navigation.findNavController(view)
        data=LyricsFragmentArgs.fromBundle(arguments!!).article!!
        lyrics=LyricsFragmentArgs.fromBundle(arguments!!).lyrics!!
        if(data!=null){
            setupViewsArticle(data,lyrics)
        }
        setupButtons()
    }

    fun setupViewsArticle(data: Data, lyrics: Lyrics){
        if(  data.album.cover!=null || data.album.cover!=""){
            Picasso.get()
                .load(data.album.cover)
                .error(R.drawable.imagen_no_disponible_)
                .into(img_foto_Article)
        }
        song.text= this.data.titleShort
        tv_title_Song.text= this.data.title
        tv_title_Artist.text= this.data.artist.name
        tv_title_Album.text= this.data.album.title
        tv_lyrics.text=lyrics.lyrics

    }
    fun setupButtons(){
        btn_back_Article.setOnClickListener {
            activity!!.onBackPressed()
        }
    }




}
