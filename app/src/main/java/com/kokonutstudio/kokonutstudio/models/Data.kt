package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    var id: Int,
    var readable: Boolean,
    var title: String,
    @SerializedName("title_short")
    var titleShort: String,
    @SerializedName("title_version")
    var titleVersion: String,
    var link: String,
    var duration: Int,
    var rank: Int,
    @SerializedName("explicit_lyrics")
    var explicitLyrics: Boolean,
    @SerializedName("explicit_content_lyrics")
    var explicitContentLyrics: Int,
    @SerializedName("explicit_content_cover")
    var explicitContentCover: Int,
    var preview: String,
    var artist: Artist,
    var album: Album,
    var type: String
):Parcelable