package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SuggestLyrics(
    var `data`: List<Data>? = arrayListOf(),
    var total: Int? = 0,
    var next: String? = ""
):Parcelable