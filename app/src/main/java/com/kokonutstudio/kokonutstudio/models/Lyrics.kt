package com.kokonutstudio.kokonutstudio.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Lyrics(
    var error: String,
    var lyrics: String

):Parcelable