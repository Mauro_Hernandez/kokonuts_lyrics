package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Album(
    var id: Int,
    var title: String,
    var cover: String,
    @SerializedName("cover_small")
    var coverSmall: String,
    @SerializedName("cover_medium")
    var coverMedium: String,
    @SerializedName("cover_big")
    var coverBig: String,
    @SerializedName("cover_xl")
    var coverXl: String,
    var tracklist: String,
    var type: String
): Parcelable