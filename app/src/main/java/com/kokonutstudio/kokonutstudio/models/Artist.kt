package com.kokonutstudio.kokonutstudio.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Artist(
    var id: Int,
    var name: String,
    var link: String,
    var picture: String,
    @SerializedName("picture_small")
    var pictureSmall: String,
    @SerializedName("picture_medium")
    var pictureMedium: String,
    @SerializedName("picture_big")
    var pictureBig: String,
    @SerializedName("picture_xl")
    var pictureXl: String,
    var tracklist: String,
    var type: String
):Parcelable